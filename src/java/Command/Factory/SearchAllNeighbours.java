/*
 * Copyright (C) 2017 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package Command.Factory;

import DTO.CityList;
import DTO.TransitPathWeightedList;
import Messages.AppWarningMessageImport;
import Messages.AppWarningMessageSearch;
import Messages.SearchResultMessage;
import Service.Neo4jService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Imports data from excel to neo4j graph database
 * @author Lech Jankowski
 */
public class SearchAllNeighbours implements Command{

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        
        String forwardTo;
        String search_source = request.getParameter("search_source");

        HttpSession session = request.getSession();
        
        if (search_source != null & search_source != "")
        {
            Neo4jService neo4js = new Neo4jService();
            TransitPathWeightedList result = neo4js.searchAllNeighbours(search_source);
        
            session.setAttribute("transits", result);
            
            if (!result.isEmpty()) {
                forwardTo = "/index.jsp";
                SearchResultMessage.setText(result.toStringFormatted());
                AppWarningMessageSearch.setText("Search neighbours for: "+search_source+" = "); 
                AppWarningMessageImport.setText("");                
            } else {
                forwardTo = "/index.jsp";
                SearchResultMessage.setText("");
                AppWarningMessageSearch.setText("There are no neighbours to: "+search_source); 
                AppWarningMessageImport.setText(""); 
                session.setAttribute("transits", null);
            }
        }
        else
        {
            forwardTo = "/index.jsp";	
            SearchResultMessage.setText("");
            AppWarningMessageSearch.setText("You must enter Source"); 
            AppWarningMessageImport.setText(""); 
            session.setAttribute("transits", null);

        }
        return forwardTo;
    }
}
