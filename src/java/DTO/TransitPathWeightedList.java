/*
 * Copyright (C) 2017 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package DTO;

import DTO.TransitPathWeighted;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Lech Jankowski
 */
public class TransitPathWeightedList {
    
    private List<TransitPathWeighted> paths;

    public TransitPathWeightedList() {
        this.paths = new ArrayList<TransitPathWeighted>();
    }

    public TransitPathWeightedList(List<TransitPathWeighted> paths) {
        this.paths = paths;
    }

    public List<TransitPathWeighted> getPaths() {
        return paths;
    }

    public void setPaths(List<TransitPathWeighted> paths) {
        this.paths = paths;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.paths);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TransitPathWeightedList other = (TransitPathWeightedList) obj;
        if (!Objects.equals(this.paths, other.paths)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TransitPathWeightedList{" + "paths=" + paths + '}';
    }
    
    public String toStringFormatted() {
        String sf = "";
        for (int i=0; i < paths.size(); i++) {
            sf = sf + paths.get(i).toStringFormatted() + "<br />";
        }
        sf = sf + "<br />";
        return sf;
    }
    
    public void addPath(TransitPathWeighted tpw) {
        this.paths.add(tpw);
    }

    public boolean isEmpty() {
        return this.paths.isEmpty();
    }
    
    
    
}
