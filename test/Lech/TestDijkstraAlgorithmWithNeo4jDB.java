/*
 * Copyright (C) 2017 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */




package Lech;

import DTO.CityList;
import DTO.TransitPathWeighted;
import DTO.TransitPathWeightedList;
import DAO.DAO_Neo4jGraphDB;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import de.vogella.algorithms.dijkstra.engine.DijkstraAlgorithm;
import de.vogella.algorithms.dijkstra.model.Edge;
import de.vogella.algorithms.dijkstra.model.Graph;
import de.vogella.algorithms.dijkstra.model.Vertex;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;


public class TestDijkstraAlgorithmWithNeo4jDB {

        private DAO_Neo4jGraphDB neo4j;
        private Graph graph;
        
        public TestDijkstraAlgorithmWithNeo4jDB() {

            // Populating Neo4j graph database with sample data
            // Note! for test reasons I've chosen the source city as London and destination city as Zurich
            // There are 2 paths: London - 1.45h - Paris - 2h - Zurich and direct London - 4.15h -Zurich
            // Shortest path it is and should produce in test is a first one.
            // Empty path where no neighborous are found is for example : Warsaw - Zurich

            neo4j = new DAO_Neo4jGraphDB();
            neo4j.eraseDatabase();
            neo4j.addCity("Zurich");
            neo4j.addCity("Dublin");
            neo4j.addCity("London");
            neo4j.addCity("Paris");
            neo4j.addCity("Zurich");
            neo4j.addCity("Warsaw");
            neo4j.addCity("Berlin");
            neo4j.addCity("Berlin"); //only distinct node is added
            neo4j.addTransit("Dublin", "London", .45);
            neo4j.addTransit("Dublin", "Paris", 2);
            neo4j.addTransit("London", "Paris", 1.45);
            neo4j.addTransit("London", "Zurich", 4.15);
            neo4j.addTransit("Paris", "Zurich", 2);
            neo4j.addTransit("Dublin", "Warsaw", 2.5);
            neo4j.addTransit("London", "Warsaw", 2);
            neo4j.addTransit("Paris", "Warsaw", 2);     
            
            graph = neo4j.convert2Graph();
        }
        

        @Test
        public void testExecute1() {
            
                // ***************************************************
                // Testing Dijkstra shortest path method
                // ****************************************************            
            
                System.out.println("Shortest path Dijkstra algorithm for London - Zurich:");            
            
                // Lets check from location London to Zurich
                DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);

                Vertex London = new Vertex("London","London");
                Vertex Zurich = new Vertex("Zurich","Zurich");
         
                dijkstra.execute(graph.getVertexes().get(graph.getVertexes().indexOf(London)));
                LinkedList<Vertex> path = dijkstra.getPath(graph.getVertexes().get(graph.getVertexes().indexOf(Zurich)));

                LinkedList<Vertex> expectedpath = new LinkedList<Vertex>();
                Vertex Paris = new Vertex("Paris","Paris");
                expectedpath.add(London);
                expectedpath.add(Paris);
                expectedpath.add(Zurich);
                
                assertNotNull(path);
                assertTrue(path.size() > 0);
                assertEquals(expectedpath, path);

                
                for (Vertex vertex : path) {
                        System.out.println(vertex);
                }
                System.out.println();
        }
        
        @Test
        public void testExecute2() {
            
                // ***************************************************
                // Testing Dijkstra empty path case
                // ****************************************************            
                
                System.out.println("Empty path Dijkstra algorithm for Warsaw - Zurich:");

                // Lets check from location Warsaw to Zurich
                DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);

                Vertex Warsaw = new Vertex("Warsaw","Warsaw");
                Vertex Zurich = new Vertex("Zurich","Zurich");
         
                dijkstra.execute(graph.getVertexes().get(graph.getVertexes().indexOf(Warsaw)));
                LinkedList<Vertex> path = dijkstra.getPath(graph.getVertexes().get(graph.getVertexes().indexOf(Zurich)));

                LinkedList<Vertex> expectedpath = null;
                
                assertEquals(expectedpath, path);

                if (path == null) 
                    System.out.println("Path empty: success!"); 
                else 
                    System.out.println("Path not empty: "+path);
                System.out.println();
        }        
        
        
        
        @Test
        public void testExecute3() {

                // ***************************************************
                // Testing Lech Jankowski's neo4j shortest path method
                // ****************************************************

                System.out.println("Shortest path Lech's Neo4j query algorithm for London - Zurich:");
            
                TransitPathWeighted neo4jpath = neo4j.getShortestPath("London", "Zurich");
                TransitPathWeighted expectedneo4jpath = new TransitPathWeighted();
                expectedneo4jpath.addCity("London");
                expectedneo4jpath.addCity("Paris");
                expectedneo4jpath.addCity("Zurich");
                expectedneo4jpath.addTransitTime(1.45);
                expectedneo4jpath.addTransitTime(2.00);
                expectedneo4jpath.setTotaltransittime(3.45);
                
                assertEquals(expectedneo4jpath, neo4jpath);
                System.out.println(neo4jpath);
                System.out.println();
        }       
        
        @Test
        public void testExecute4() {

                // ***************************************************
                // Testing Lech Jankowski's neo4j empty path case
                // ****************************************************

                System.out.println("Empty path Lech's Neo4j query algorithm for Warsaw - Zurich:");
                
                TransitPathWeighted neo4jpath = neo4j.getShortestPath("Warsaw", "Zurich");
                TransitPathWeighted expectedneo4jpath = new TransitPathWeighted();
                
                assertEquals(expectedneo4jpath, neo4jpath);
                System.out.println(neo4jpath);
                System.out.println();
        }         
        
        @Test
        public void testExecute5() {
            
            // testing new methods isEmpty() for custom data structure used in neo4jDB functionality
            
            CityList cities = new CityList();
            TransitPathWeighted tpw = new TransitPathWeighted();
            TransitPathWeightedList tpwl = new TransitPathWeightedList();
            
            assertEquals(true, cities.isEmpty());
            assertEquals(true, tpw.isEmpty());
            assertEquals(true, tpwl.isEmpty());
        }
        
}